# bil.anark.ist

**Project is Work In Progress!**

## Project specific setup

This is how I started the project, to resume development simply have vue installed and run ``yarn install``.

```
npm install -g @vue/cli
vue create bil.anark.ist
```

**In vue create**: Select Custom preferences and choose Typescript, otherwise go with defaults.

```
cd bil.anark.ist
vue add vuetify
vue add router
vue add vuex
yarn add @mdi/js -D
```

## Project specific design

An example flow chart for a typical user session.

* Landing page is ``src/views/StartQuiz.vue``.
* StartQuiz selects the type of quiz to run and launches the quiz.
* StartQuiz loads every Component in ``src/views/questions`` by loading the questions.ts file in each category.
* StartQuiz then selects the number of questions you requested (n) from these and populates a list of child-routes.
* Each child-route has a path indexed 1-n to avoid giving away info about the questions.
* StartQuiz finally sends you to the parent route Quiz that now has a list of child-routes under it.
* Quiz shows a back and forward navigation arrow for all its child-routes.
' Quiz shows an answer form with answers from each Component randomized.
* Answering a question stores the answer in vuex state.
* At end of Quiz go through correct answers and show result to user.
* At end of Quiz depopulate list of child-routes back to 0.
* Return to StartQuiz.

## Roadmap

- [ ] Dynamically load quiz questions and randomize X of them into the routes as children of the Quiz route.
- [ ] Store quiz state in vuex, current question, answers, score.
- [ ] Finish Quiz "wizard" and display scoring results.
- [ ] Always add more questions and categories.
- [ ] Write guide for adding new questions.
- [ ] Open repo and announce on social media for people to start adding questions.
- [x] Use Nord [theme](https://vuetifyjs.com/en/features/theme/) for dark and perhaps Solarized light for light theme.
- [ ] [i18n support](https://vuetifyjs.com/en/features/internationalization/).
- [ ] See what we can do for [accessibility](https://vuetifyjs.com/en/features/accessibility/).

# Vue-cli generated README

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
