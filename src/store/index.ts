import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const QUIZ_STARTED = 'quiz started'
export const QUIZ_STOPPED = 'quiz stopped'
export const START_QUIZ = 'start quiz'
export const STOP_QUIZ = 'stop quiz'
export const ANSWER_QUESTION = 'answer question'

export default new Vuex.Store({
  state: {
    questions: [],
    quizStarted: false
  },

  mutations: {
    [QUIZ_STARTED]: (state, questions) => {
      state.questions = questions
      state.quizStarted = true
    },
    [QUIZ_STOPPED]: (state) => {
      state.questions = []
      state.quizStarted = false
    },
    [ANSWER_QUESTION]: (state) => {
      console.log(state.questions)
    }
  },

  actions: {
    [START_QUIZ]: ({ commit }, questions) => {
      commit(QUIZ_STARTED, questions)
    },
    [STOP_QUIZ]: ({ commit }) => {
      commit(QUIZ_STOPPED)
    },
    [ANSWER_QUESTION]: ({ commit }, { index, answer }) => {
      commit(ANSWER_QUESTION)
    }
  }
})
