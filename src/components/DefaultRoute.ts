import Vue from 'vue'

export default Vue.component(
  'default-route',
  {
    template: '',
    created: () => {
      window.location.href = '/'
    }
  }
)
