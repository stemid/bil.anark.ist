import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import StartQuiz from '@/views/StartQuiz.vue'
import Quiz from '@/views/Quiz.vue'
import DefaultRoute from '@/components/DefaultRoute.ts'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'StartQuiz',
    component: StartQuiz
  },
  {
    path: '/quiz',
    name: 'Quiz',
    component: Quiz,
    children: []
  }
/*
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () => import(/\* webpackChunkName: "about" *\/ '../views/About.vue')
  }
*/
]

const router = new VueRouter({
  routes
})

export default router
