import { Component } from 'vue'

export interface QuestionAnswer {
  text: string,
  correct?: boolean
}

export interface QuestionItem {
  category: string,
  name: string,
  component: Component
}

