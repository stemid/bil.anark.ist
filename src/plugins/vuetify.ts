import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    dark: true,
    themes: {
      dark: {
        background: '#2e3440',
        primary: '#5e81ac',
        secondary: '#d8dee9',
        accent: '#e5e9f0',
        error: '#bf616a',
        warning: '#d08770',
        success: '#a3d48e'
      },
      light: {
        background: '#e5e9f0',
        primary: '#5e81ac',
        secondary: '#d8dee9',
        accent: '#2e3440',
        error: '#bf616a',
        warning: '#d08770',
        success: '#759c65'
      }
    }
  },
  icons: {
    iconfont: 'mdi'
  }
})
