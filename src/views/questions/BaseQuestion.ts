import Vue from 'vue'
import { mapState } from 'vuex'
import { QuestionAnswer } from '@/types'
import QuestionAnswerForm from '@/components/QuestionAnswerForm.vue'
import { ANSWER_QUESTION } from '@/store'

export default Vue.extend({
  name: 'BaseQuestion',
  components: {
    QuestionAnswerForm
  },

  data: () => ({
    answers: []
  }),

  computed: {
    ...mapState([
      'quizStarted',
      'questions'
    ])
  },

  methods: {
    answerForm (answer: Array<QuestionAnswer>) {
      this.$store.dispatch(ANSWER_QUESTION, 0, answer)
    },

    shuffleAnswers () {
      for (let i = this.answers.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [this.answers[i], this.answers[j]] = [this.answers[j], this.answers[i]]
      }
    }
  },

  created () {
    this.shuffleAnswers()
  }
})
