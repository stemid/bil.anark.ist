import { QuestionItem } from '@/types'
import Category from './index'
import A7 from './A7.vue'
import A12 from './A12.vue'

export const questions: Array<QuestionItem> = [
  {
    category: Category.name,
    name: 'A12',
    component: A12
  },
  {
    category: Category.name,
    name: 'A7',
    component: A7
  }
]
