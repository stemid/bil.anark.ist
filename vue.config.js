module.exports = {
  /*
   * This would disable tslinter completely
  chainWebpack: (config) => {
    config.plugins.delete('fork-ts-checker')
  },
  */
  parallel: true,
  transpileDependencies: [
    'vuetify'
  ]
}
